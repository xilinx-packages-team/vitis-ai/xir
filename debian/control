Source: xir
Maintainer: Debian Xilinx Package Maintainers <team+pkg-xilinx@tracker.debian.org>
Uploaders: Punit Agrawal <punit@debian.org>,
           Nobuhiro Iwamatsu <iwamatsu@debian.org>
Priority: optional
Section: libs
Build-Depends: cmake, debhelper-compat (= 13),
	libboost-dev,
	libprotobuf-dev,
	libssl-dev,
	libunilog-dev (>= 2.5),
	protobuf-compiler
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/Xilinx/Vitis-AI
Vcs-Git: https://salsa.debian.org/xilinx-packages-team/vitis-ai/xir.git
Vcs-Browser: https://salsa.debian.org/xilinx-packages-team/vitis-ai/xir

Package: libxir2
Architecture: amd64 arm64 armhf armel
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Xilinx Intermediate Representation (XIR) for deep learning algorithms (runtime)
 Xilinx Intermediate Representation (XIR) is a graph based
 intermediate representation of the AI algorithms which is well
 designed for compilation and efficient deployment of the
 Domain-specific Processing Unit (DPU) on the FPGA platform. Advanced
 users can apply Whole Application Acceleration to benefit from the
 power of FPGA by extending the XIR to support customized IP in Vitis
 AI flow.
 .
 XIR includes Op, Tensor, Graph and Subgraph libraries, which
 providing a clear and flexible representation for the computational
 graph. For now, it's the foundation for the Vitis AI quantizer,
 compiler, runtime and many other tools. XIR provides in-memory
 format, and file format for different usage. The in-memory format XIR
 is a Graph object, and the file format is a xmodel. A Graph object
 can be serialized to a xmodel while the xmodel can be deserialized to
 the Graph object.
 .
 In the Op library, there's a well-defined set of operators to cover
 the wildly used deep learning frameworks, e.g. TensorFlow, Pytorch
 and Caffe, and all of the built-in operators for DPU. This enhences
 the expression ability and achieves one of the core goals of
 eliminating the difference between these frameworks and providing a
 unified representation for users and developers.
 .
 XIR also provides a Python APIs which is named PyXIR. It enables
 Python users to fully access XIR and benefits in a pure Python
 environment, e.g. co-develop and integrate users' Python project with
 the current XIR based tools without massive dirty work to fix the gap
 between two languages.
 .
 This package provides the runtime environment for XIR.

Package: libxir-dev
Architecture: amd64 arm64 armhf armel
Section: libdevel
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}, libxir2 (= ${binary:Version})
Description: Xilinx Intermediate Representation (XIR) for deep learning algorithms (develop)
 Xilinx Intermediate Representation (XIR) is a graph based
 intermediate representation of the AI algorithms which is well
 designed for compilation and efficient deployment of the
 Domain-specific Processing Unit (DPU) on the FPGA platform. Advanced
 users can apply Whole Application Acceleration to benefit from the
 power of FPGA by extending the XIR to support customized IP in Vitis
 AI flow.
 .
 XIR includes Op, Tensor, Graph and Subgraph libraries, which
 providing a clear and flexible representation for the computational
 graph. For now, it's the foundation for the Vitis AI quantizer,
 compiler, runtime and many other tools. XIR provides in-memory
 format, and file format for different usage. The in-memory format XIR
 is a Graph object, and the file format is a xmodel. A Graph object
 can be serialized to a xmodel while the xmodel can be deserialized to
 the Graph object.
 .
 In the Op library, there's a well-defined set of operators to cover
 the wildly used deep learning frameworks, e.g. TensorFlow, Pytorch
 and Caffe, and all of the built-in operators for DPU. This enhences
 the expression ability and achieves one of the core goals of
 eliminating the difference between these frameworks and providing a
 unified representation for users and developers.
 .
 XIR also provides a Python APIs which is named PyXIR. It enables
 Python users to fully access XIR and benefits in a pure Python
 environment, e.g. co-develop and integrate users' Python project with
 the current XIR based tools without massive dirty work to fix the gap
 between two languages.
 .
 This package provides the development environment for XIR.

Package: libxir-utils
Architecture: amd64 arm64 armhf armel
Section: utils
Depends: ${misc:Depends}, ${shlibs:Depends}, libxir2 (= ${binary:Version})
Description: Xilinx Intermediate Representation (XIR) for deep learning algorithms (utils)
 Xilinx Intermediate Representation (XIR) is a graph based
 intermediate representation of the AI algorithms which is well
 designed for compilation and efficient deployment of the
 Domain-specific Processing Unit (DPU) on the FPGA platform. Advanced
 users can apply Whole Application Acceleration to benefit from the
 power of FPGA by extending the XIR to support customized IP in Vitis
 AI flow.
 .
 XIR includes Op, Tensor, Graph and Subgraph libraries, which
 providing a clear and flexible representation for the computational
 graph. For now, it's the foundation for the Vitis AI quantizer,
 compiler, runtime and many other tools. XIR provides in-memory
 format, and file format for different usage. The in-memory format XIR
 is a Graph object, and the file format is a xmodel. A Graph object
 can be serialized to a xmodel while the xmodel can be deserialized to
 the Graph object.
 .
 In the Op library, there's a well-defined set of operators to cover
 the wildly used deep learning frameworks, e.g. TensorFlow, Pytorch
 and Caffe, and all of the built-in operators for DPU. This enhences
 the expression ability and achieves one of the core goals of
 eliminating the difference between these frameworks and providing a
 unified representation for users and developers.
 .
 XIR also provides a Python APIs which is named PyXIR. It enables
 Python users to fully access XIR and benefits in a pure Python
 environment, e.g. co-develop and integrate users' Python project with
 the current XIR based tools without massive dirty work to fix the gap
 between two languages.
 .
 This package contains the utilities from XIR.
